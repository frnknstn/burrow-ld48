extends TileMap


var player_last_dig = {}

var touch_active = false


#func _ready():
#	pass # Replace with function body.


func _unhandled_input(event):
	# left click to move
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				touch_active = true
				send_move_order()
			else:
				touch_active = false
			get_tree().set_input_as_handled()
	elif event is InputEventMouseMotion:
		if touch_active:
			send_move_order()
			get_tree().set_input_as_handled()


func send_move_order():
	get_node("/root/Main/Player").set_target(get_local_mouse_position())


func dig_hole(pos: Vector2, tile: int, radius: float = 4.0) -> Array:
	# fill a small hole with a tile spec using a naieve circle draw thingie
	# returns a list of everything that got dug
	var contents = []
	var int_radius = ceil(radius) as int
	
	for y in range(pos.y - int_radius, pos.y + int_radius):
		for x in range(pos.x - int_radius, pos.x + int_radius):
			if pos.distance_to(Vector2(x, y)) <= (0.9 * radius):
				# match the type of dirt
				if get_cell(x, y) != -1:
					contents.append(GameDefs.DirtTypes.DIRT)
					set_cell(x, y, tile)
	
	return contents


func _on_Player_dig_hole(player, pos: Vector2, radius: float):
	# basic performance optimisation
	if player_last_dig.get(player, null) == [pos, radius]:
		return
		pass
	player_last_dig[player] = [pos, radius]
	
	# do the dig
	pos = world_to_map(pos)
	var contents = dig_hole(pos, -1, radius)
	for item in contents:
		player.add_action([GameDefs.PlayerActions.DIG, item])
		
	
