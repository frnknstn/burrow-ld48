extends Area2D

signal food_value_changed

var food_value: float = 0.0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


# tally all the contained food
func _on_Home_area_entered(area):
	if area.is_food():
		food_value += area.food_value
		emit_signal("food_value_changed")


func _on_Home_area_exited(area):
	if area.is_food():
		food_value -= area.food_value
		emit_signal("food_value_changed")
