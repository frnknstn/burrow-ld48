extends Camera2D

onready var target = get_node("/root/Main/Player")

# Called when the node enters the scene tree for the first time.
func _ready():
	# set our limits
	limit_left = GameDefs.PLAY_AREA_LEFT
	limit_right = GameDefs.PLAY_AREA_LEFT + GameDefs.PLAY_AREA_WIDTH
	limit_top = GameDefs.PLAY_AREA_TOP - 50
	if GameDefs.PLAY_AREA_HEIGHT != INF:
		limit_bottom = GameDefs.PLAY_AREA_TOP + GameDefs.PLAY_AREA_HEIGHT
	else:
		limit_bottom = 1_000_000_000


func _process(delta):
	position = target.position
