extends CanvasLayer


onready var hunger_bar: ProgressBar = $TopBox/Row1/HungerUI/HungerBar
onready var inventory_label: Label = $TopBox/Row2/InventoryLabel
onready var stored_hint_label: Label = $TopBox/Row3/StoredHintLabel
onready var stored_label: Label = $TopBox/Row3/StoredLabel
onready var winter_timer_label: Label = $TopBox/Row1/WinterTimerLabel

const HUNGER_LOW_COLOR = Color(0.75, 0.9, 0.30)
const HUNGER_HIGH_COLOR = Color(0.9, 0.45, 0.30)
const INVENTORY_NORMAL_COLOR = Color(1, 1, 1)
const INVENTORY_FULL_COLOR = Color(1, 0.3, 0.3)

static func fake_action(s):
	"Fake an action in the most believable way"
	# satisfy is_action_pressed and is_action_just_pressed
	Input.action_press(s)
	
	# satisfy _input etc
	var event = InputEventAction.new()
	event.action = s
	event.pressed = true
	Input.parse_input_event(event)


# Called when the node enters the scene tree for the first time.
func _ready():
	update_hunger_hint()
	if OS.has_touchscreen_ui_hint():
		$TouchPanel.visible = true
	else:
		$TouchPanel.visible = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func update_hunger_hint():
	# make sure the hunger hint is at the right spot
	# TODO wtf resize
	var hunger_hint_offset = (hunger_bar.margin_right - hunger_bar.margin_left) * GameDefs.GREEDY_HUNGER
	hunger_bar.get_node("veg_a").position = Vector2(
		hunger_hint_offset, 
		(hunger_bar.margin_bottom - hunger_bar.margin_top) / 2
	)


func _on_Player_hunger_changed(val: float):
	var style: StyleBoxFlat = hunger_bar.get("custom_styles/fg")
	hunger_bar.value = val
	if val <= GameDefs.GREEDY_HUNGER * 100:
		style.bg_color = HUNGER_LOW_COLOR
	else:
		style.bg_color = HUNGER_HIGH_COLOR
	
	update_hunger_hint()


func _on_Player_inventory_changed(inv: Array):
	var food: float = 0.0
	for item in inv:
		food += item
	
	if food < GameDefs.MAX_INVENTORY_SIZE:
		inventory_label.add_color_override("font_color", INVENTORY_NORMAL_COLOR)
	else:
		inventory_label.add_color_override("font_color", INVENTORY_FULL_COLOR)
	
	inventory_label.text = "%d / %d food" % [food / 1000, GameDefs.MAX_INVENTORY_SIZE / 1000]


func _on_Main_stored_food_total_changed(food: float):
	stored_hint_label.text = "Store %d food to win:" % [GameDefs.storage_goal / 1000]
	stored_label.text = "%d / %d food" % [food / 1000, GameDefs.storage_goal / 1000]


func _on_Main_winter_timer_changed(secs): 
	secs = ceil(secs) as int
	winter_timer_label.text = "%d:%02d" % [floor(secs / 60), fmod(secs, 60)]


func _on_FoodButton_pressed():
	fake_action("ui_accept")


func _on_StorageButton_pressed():
	fake_action("ui_focus_next")
