extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var ui_head = $CanvasLayer/VBoxContainer


# Called when the node enters the scene tree for the first time.
func _ready():
	var story = ""
	var heading = "Game Over!"
	
	# work out what happened
	match GameDefs.game_result:
		GameDefs.GameResult.STARVED:
			story = "Poor little magic mole fell asleep before they could collect enough food for winter :("
			$LoseSound.play()
		GameDefs.GameResult.STORAGE_BARE:
			story = "Magic mole made it to winter, but doesn't have enough food stored. It's going to be a hungry winter :("
			$LoseSound.play()
		GameDefs.GameResult.WON:
			story = "Magic mole collected enough food to last all winter!"
			heading = "You Win!"
			$WinSound.play()
			

	# populate the game over screen
	ui_head.get_node("GameName").text = GameDefs.TITLE
	ui_head.get_node("Story").text = story
	ui_head.get_node("ResultText").text = heading
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	get_tree().change_scene("res://menu.tscn")
