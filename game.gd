extends Node

const TITLE = "Burrow - Ludum Dare 48 Jam"

const PLAY_AREA_LEFT: int = 0
const PLAY_AREA_WIDTH: int = 1920
const PLAY_AREA_TOP: int = 0
const PLAY_AREA_HEIGHT = 1650

const GREEDY_HUNGER = 0.25
const MAX_INVENTORY_SIZE = 10_000

const MIN_HOME_DISTANCE = 800


# enums
enum PlayerActions {
	DIG,
	MOVE,
}

enum DirtTypes {
	DIRT,
}

enum GameResult {
	NONE,
	STARVED,
	STORAGE_BARE,
	WON,
}


# level params
var level = 1
var winter_time = 180
var storage_goal = 30_000
var game_result = GameResult.NONE
var item_count = 80
var water_count = 14


