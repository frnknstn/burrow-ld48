extends Node2D

export var count: int = 20

var WaterDrop = preload("waterdrop.tscn")
var open = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	if not open:
		return
		
	add_child(WaterDrop.instance())
	
	# are we are done?
	count -= 1
	if count <= 0:
		$Timer.stop()


func _on_Area2D_area_entered(area):
	return
