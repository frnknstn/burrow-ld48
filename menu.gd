extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$CanvasLayer/TextureRect/Instructions.visible = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_StartButton_pressed():
	$CanvasLayer/StartButton.text = "Now Loading..."
	yield(get_tree().create_timer(0.05), "timeout")
	get_tree().change_scene("res://main.tscn")


func _on_ExitButton_pressed():
	get_tree().quit()


func _on_InfoButton_pressed():
	$CanvasLayer/TextureRect/Instructions.visible = not $CanvasLayer/TextureRect/Instructions.visible

