extends Area2D

signal dig_hole		# (pos: Vector2, radius: float)
signal hunger_changed	# (percent: float)
signal inventory_changed	# (inv: Array)
signal player_error	# (s: str, pos: Vector2)


export var speed: int = 100

onready var target: Vector2 = position

var hunger: float = 0.0
var max_hunger: float = 10000.0
var min_hunger: float = -1000.0	# extra belly space
var greedy: bool = false

var action_log = []

var inventory = []

# imports
var PlayerActions = GameDefs.PlayerActions
var DirtTypes = GameDefs.DirtTypes

var Feature = preload("res://features/feature.gd")
var Crate = preload("res://features/crate.tscn")
var Home = preload("res://home.tscn")
var TextFloater = preload("res://text_floater.tscn")


# Functions
func _ready():
	# prime the UI
	pass


func _process(delta):
	# process input
	if Input.is_action_just_pressed("ui_select"):
		dump_inventory()
	if Input.is_action_just_pressed("ui_focus_next") or Input.is_action_just_pressed("ui_accept"):
		build_home()
	
	# movement processing
	var dir = Vector2(0, 0)
	var velocity = Vector2(0, 0)
	var distance_to_target = 0
	
	# key / stick move code
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed * delta
		target = position + velocity
	
		# constrain to playable area:
		var constrained: bool = false
		if target.x < GameDefs.PLAY_AREA_LEFT:
			target.x = GameDefs.PLAY_AREA_LEFT
			constrained = true
		elif target.x > GameDefs.PLAY_AREA_LEFT + GameDefs.PLAY_AREA_WIDTH:
			target.x = GameDefs.PLAY_AREA_LEFT + GameDefs.PLAY_AREA_WIDTH
			constrained = true
		if target.y < GameDefs.PLAY_AREA_TOP:
			target.y = GameDefs.PLAY_AREA_TOP
			constrained = true
		elif target.y > GameDefs.PLAY_AREA_TOP + GameDefs.PLAY_AREA_HEIGHT:
			target.y = GameDefs.PLAY_AREA_TOP + GameDefs.PLAY_AREA_HEIGHT
			constrained = true
		
		if constrained:
			velocity = target - position
		
		distance_to_target = 0
	else:
		# enable mouse mode
		distance_to_target = position.distance_to(target)
	
	# mouse / touch move code
	if distance_to_target > 0 and distance_to_target < 1:
		# short hop
		position = target
	elif distance_to_target >= 1:
		# normal move
		dir = (target - position).normalized()
		velocity = dir * speed * delta
		
		# don't overshoot
		if velocity.length() > distance_to_target:
			velocity = target - position
			
	# do the move
	if velocity.length():
		look_at(position + velocity)
		rotation += deg2rad(-90)
		position += velocity
		
		emit_signal("dig_hole", self, $Head.global_position, 3.5)
		add_action([PlayerActions.MOVE, velocity.length()])
		
		if distance_to_target == 0:
			# not in mouse mode, clear our target
			target = position
		
	# calculate our hunger
	var hunger_change = 0.0
	for action_details in action_log:
		var action = action_details[0]
		var details = action_details[1]
		match action:
			PlayerActions.DIG:
				match details:
					DirtTypes.DIRT:
						hunger_change += 0.75
			PlayerActions.MOVE:
				hunger_change += details
	if hunger_change != 0:
		add_hunger(hunger_change)
	action_log = []


func set_target(pos: Vector2):
	if pos == target:
		return
	
	# update our move target
	target = pos
	
	# constrain
	if target.x < GameDefs.PLAY_AREA_LEFT:
		target.x = GameDefs.PLAY_AREA_LEFT
	elif target.x > GameDefs.PLAY_AREA_WIDTH:
		target.x = GameDefs.PLAY_AREA_WIDTH
	
	if target.y < GameDefs.PLAY_AREA_TOP:
		target.y = GameDefs.PLAY_AREA_TOP
	if target.y > GameDefs.PLAY_AREA_HEIGHT:
		target.y = GameDefs.PLAY_AREA_HEIGHT
	

func add_hunger(val: float):
	hunger += val
	if hunger < min_hunger:
		hunger = min_hunger
	
	emit_signal("hunger_changed", hunger / max_hunger * 100)
	
	# check if we have changed to greedy
	if hunger >= (GameDefs.GREEDY_HUNGER * max_hunger):
		if not greedy:
			# newly greedy
			$GreedyPlayer.play()
			greedy = true
	else:
		# not greedy
		greedy = false
	
	if hunger >= max_hunger:
		# bad end
		GameDefs.game_result = GameDefs.GameResult.STARVED
		get_tree().change_scene("res://game_over.tscn")
	
	
func add_action(action):
	# add a new action to our action log
	self.action_log.append(action)


func dump_inventory():
	if inventory.empty():
		error_text("not carrying any food")
	while not inventory.empty():
		$ThrowSound.play()
		var item = inventory.pop_back()
		# toss one food per 1000 KJ
		while item > 0:
			var crate = Crate.instance()
			get_node("/root/Main/GroundMap").add_child(crate)
			crate.launch(global_position, 1000)
			item -= 1000
		emit_signal("inventory_changed", inventory)
		yield(get_tree().create_timer(0.333), "timeout")


func build_home():
	# check cooldown
	if not $BuildTimer.is_stopped():
		error_text("too soon since last storage")
		return
	
	# check site
	if global_position.y < 300:
		error_text("storage must be deeper underground")
		return
		
	for home in get_tree().get_nodes_in_group("homes"):
		if global_position.distance_to(home.global_position) < GameDefs.MIN_HOME_DISTANCE:
			error_text("too close to other storage")
			return
			
	# TODO: maybe actually check area
	
	# do the build
	$BuildTimer.start()
	var home = Home.instance()
	home.position = global_position
	get_node("/root/Main").add_child_below_node($"/root/Main/GroundBackdrop", home)
	home.connect("food_value_changed", $"/root/Main", "_on_House_food_total_changed")


func _on_Player_area_shape_entered(area_id, area, area_shape, local_shape):
	# visibility
	var my_shape = shape_owner_get_owner(local_shape)
	
	if area is Feature:
		area.discover()
	
	# pickups
	if my_shape.name == "TouchCollider":
		if area.is_food():
			if hunger >= (GameDefs.GREEDY_HUNGER * max_hunger):
				play_noms(area.food_value)
				if area.get_shadow_size() > 0:
					emit_signal("dig_hole", self, area.global_position, area.get_shadow_size() * 0.28)
				add_hunger(-area.eat())
			elif Utils.sum(inventory) < GameDefs.MAX_INVENTORY_SIZE:
				# pick up the food
				$GetSound.play()
				var food_value: float = area.pick_up()
				while food_value > 0:
					inventory.append(1000)
					food_value -= 1000
				emit_signal("inventory_changed", inventory)
		elif area.name == "SpigotArea":
			if not area.get_node("..").open:
				area.get_node("..").open = true
				area.get_node("../SplashPlayer").play()
				emit_signal("dig_hole", self, area.global_position, 2.3)
			

func play_noms(food_value):
	"Display the appropriate amount of enthusiasm for a meal"
	while food_value > 0:
		food_value -= 1000
		$EatSound.play()
		yield(get_tree().create_timer(0.2), "timeout")


func error_text(s):
	"draw floating error text"
#	emit_signal("player_error", s, self.get_global_transform_with_canvas().get_origin())
	var msg = TextFloater.instance()
	get_node("..").add_child(msg)
	msg.set_text(s, get_global_transform().origin)

