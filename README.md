## Burrow - LD48 Game Jam #

You are a magic mole, dig for food, dig deeper to escape the frost.

### Ideas #

* Maybe 3 types of mole?
  * Omnivore
  * Carnivore
  * Herbivore
* Click to move / dig
* Roots are safe food, but dig further away for better rewards?

### Todo #

Like 2 days in and finally I have a hard concept 


### Post-Jam improvements:

* ~Changed fonts~
* ~improved GUI~
* small dig when discovering water
* instructions re: the storage area not having a floor
* ~a few touchscreen-friendly buttons~
* ~my poor little helpless harmless beautiful blue block has been marked for death by my heartless co-creator ;(~
    * direct quote: "NO MERCY"
* nice to have:
	* custom game modes
	* zen mode
		* "press q to quit"

#### Basics / Now working on #

* MVP
	* carry food
	* drop food
	* Button to make home
	* Winter timer
	* Game over screen (loss / score)
	* Random food location

#### More details #

* Hunger bar (fancier)
* Eat food to fill
* Carry food when not hungry
* Need to "store" food for winter
	* areas automatically designated as "storage"
* As game goes on, winter comes in and starts freezing ground
* Ice makes it harder to dig
* Random Map Generation

Nice to have:
* Hazards
	* Falling rocks
	* Water?
		* maybe spoils food?
* Some way to encorage risk-taking?

#### Done #

* hidden objects
* eat food

### Details #

* ground
	* 4x4 tile map
	