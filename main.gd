extends Node2D

#onready var GameDefs = get_node("/root/GameDefs")

var VegA = preload("res://features/veg_a.tscn")
var VegB = preload("res://features/veg_b.tscn")
var WaterSource = preload("res://watersource.tscn")


signal stored_food_total_changed	# (food: float)
signal winter_timer_changed	# (secs: float)

var stored_food: float = 0.0
var winter_time_left = 0.0
var end_scene = preload("res://game_over.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	generate_items()
	
	# start the countdown
	winter_time_left = GameDefs.winter_time
	$WinterTimer.wait_time = winter_time_left
	$WinterTimer.start()
	
	# prime the UI	
	emit_signal("stored_food_total_changed", stored_food)
	emit_signal("winter_timer_changed", winter_time_left)
	
	$MusicPlayer.play()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# show FPS
	OS.set_window_title(GameDefs.TITLE + " - " + str(Engine.get_frames_per_second()) + " fps")
	
	if ceil($WinterTimer.time_left) != winter_time_left:
		winter_time_left = ceil($WinterTimer.time_left)
		emit_signal("winter_timer_changed", winter_time_left)
	

func generate_items():
	"Populate the level with items"
	for i in range(GameDefs.item_count):
		var x = rand_range(GameDefs.PLAY_AREA_LEFT, GameDefs.PLAY_AREA_LEFT + GameDefs.PLAY_AREA_WIDTH)
		var y = rand_range(GameDefs.PLAY_AREA_TOP, GameDefs.PLAY_AREA_TOP + GameDefs.PLAY_AREA_HEIGHT)
		var pos = Vector2(x, y)
		
		var item
		match randi() % 5:
			0, 1, 2:
				item = VegA.instance()
			3, 4:
				item = VegB.instance()
				
		get_node("/root/Main").add_child(item)
		item.position = pos
	
	for i in range(GameDefs.water_count):
		var x = rand_range(GameDefs.PLAY_AREA_LEFT, GameDefs.PLAY_AREA_LEFT + GameDefs.PLAY_AREA_WIDTH)
		var y = rand_range(GameDefs.PLAY_AREA_TOP, GameDefs.PLAY_AREA_TOP + GameDefs.PLAY_AREA_HEIGHT)
		var pos = Vector2(x, y)
		
		var item = WaterSource.instance()
		item.count = 15 + (randi() % 15)
		
		get_node("/root/Main").add_child(item)
		item.position = pos


func _on_House_food_total_changed():
	stored_food = 0
	for home in get_tree().get_nodes_in_group("homes"):
		stored_food += home.food_value
	
	emit_signal("stored_food_total_changed", stored_food)
	
	# check for victory
	if stored_food >= GameDefs.storage_goal:
		yield(get_tree().create_timer(2), "timeout")
		
		# check we are stilll winners
		if stored_food >= GameDefs.storage_goal:
			game_over(GameDefs.GameResult.WON)


func winter_starts():
	winter_time_left = 0.0
	emit_signal("winter_timer_changed", winter_time_left)
	
	# record our result
	if stored_food < GameDefs.storage_goal:
		game_over(GameDefs.GameResult.STORAGE_BARE)
	else:
		game_over(GameDefs.GameResult.WON)


func game_over(result):
	GameDefs.game_result = result
	get_tree().change_scene_to(end_scene)


func _on_WinterTimer_timeout():
	winter_starts()
