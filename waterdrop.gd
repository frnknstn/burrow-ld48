extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var Crate = preload("res://features/crate.gd")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	z_index = min(position.y, 1_000_000.0)
	
	# budget billboard
	$Sprite.rotation = -rotation
	


func _on_WaterDrop_sleeping_state_changed():
	set_process(!sleeping)


func _on_WaterDrop_body_entered(body):
	if body is Crate:
		# hurt the food
		$PainPlayer.play()
		body.queue_free()
