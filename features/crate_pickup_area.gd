# emulate a Feature

extends Area2D

var food_value: float

func is_food():
	return true

func eat() -> float:
	"""Consume this item, returning its food value"""
	$"..".queue_free()
	return food_value


func pick_up()  -> float:
	"""Pick up this item, returning its food value"""
	$"..".queue_free()
	return food_value


func get_shadow_size() -> float:
	"return an approximate dirt radius of this item"
	return 0.0
