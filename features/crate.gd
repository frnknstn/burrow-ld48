extends RigidBody2D

var food_value: float = 0.0
var portable: bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func launch(pos: Vector2, food: float):
	set_food_value(food)
	position = pos
	
	# kick off at random
	$Sprite.rotation_degrees = rand_range(0, 360)
	apply_central_impulse(Vector2(rand_range(-0.5, 0.5), -0.75) * 80)
	set_angular_velocity(rand_range(-1, 1) * 35)
	
	# only pickup after timeout
	yield(get_tree().create_timer(2.5), "timeout")
	$PickupArea.monitorable = true

func set_food_value(food):
	food_value = food
	$PickupArea.food_value = food
