extends Area2D

"""Basic hidden object on the map"""

export var hidden: bool = true
export var fade_time: float = 1.0
export var food_value: float = 0.0 setget , food_value_get

var _fade_t = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	# only use process for smooth fade
	set_process(false)
	
	# start invisible
	if hidden:
		modulate.a = 0
		
	rotation_degrees = rand_range(0, 360)


func _process(delta):
	# fade in
	modulate.a = lerp(modulate.a, 1.0, delta * 1.75)
	if modulate.a >= 0.95:
		modulate.a = 1.0
		set_process(false)


func is_food() -> bool:
	return (food_value > 0.0)
	

func discover():
	"""show this object now"""
	if hidden:
		set_process(true)


func eat() -> float:
	"""Consume this item, returning its food value"""
	self.queue_free()
	return food_value


func pick_up()  -> float:
	"""Pick up this item, returning its food value"""
	self.queue_free()
	return food_value


func food_value_get():
	return food_value


func get_shadow_size() -> float:
	"return an approximate radius of this item"
	var dim = $Sprite.texture.get_size() * $Sprite.transform.get_scale()
	return (dim.x + dim.y) / 4
