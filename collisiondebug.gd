extends Label


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_GroundMap_mouse_over(source):
	text = "mouse: g" + str(get_global_mouse_position()) + " cell " + str(source)


func _on_Player_tile_collision(collision: KinematicCollision2D, tile: Vector2):
	text = "collision: pos " + str(collision.position) + " normal " + str(collision.normal) +  " tile " + str(tile)
